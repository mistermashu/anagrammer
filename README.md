General
=======

This is a simple little program that finds anagrams.


Algorithm
=========

1. Loads up all words by reading the file /usr/share/dict/words.
2. Stores words from the dictionary file in a hash map, where the *sorted* words are the keys, and the list of all words that are anagrams of the key as the value.
3. When the user enters an anagram to solve, it sorts the input and simply looks it up in the hash map.


Contributors
============

- Matt Larson (mistermashu.com)


TODO
====

- make it so you can pass it a file as a parameter and it solves all anagrams in that file.
- make it so each line in the file (and each line in standard input) can have multiple words. it should look them up individually.